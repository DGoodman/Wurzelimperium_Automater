import requests
import random
import json



class wurzel_session:

    '''
    LOGIN INFORMATION
    '''
    username = ""
    password = ""
    server = ""

    '''
    SESSION INFORMATION
    '''
    WUNR = ""
    PHPSESSID = ""
    AJAXTOKEN = ""

    '''
    ACCOUNT INFORMATION
    '''
    money = 0
    points = 0
    inventory = {}
    garden = {}

    '''
    GENERAL INFORMATION
    '''
    products = {}



    def __init__(self, user, password, server):
        self.username = user
        self.password = password
        self.server = server








    '''
    Logs in to the account -> gets WUNR and PHPSESSID
    '''
    def login(self):
        headers = {
            'Accept': 'text/javascript, text/html, application/xml, text/xml, */*',
            'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
            'Cache-Control': 'no-cache',
            'Connection': 'keep-alive',
            'Content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Origin': 'https://www.wurzelimperium.de',
            'Pragma': 'no-cache',
            'Referer': 'https://www.wurzelimperium.de/',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-origin',
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.88 Safari/537.36',
            'X-Prototype-Version': '1.7',
            'X-Requested-With': 'XMLHttpRequest',
            'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="100", "Google Chrome";v="100"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Linux"',
        }

        params = {
            'r': str(random.randint(100000000, 999999999)),
        }

        data = {
            'do': 'login',
            'server': "server" + self.server,
            'user': self.username,
            'pass': self.password,
        }

        response = requests.post('https://www.wurzelimperium.de/dispatch.php', params=params, headers=headers, data=data)

        url = response.json()["url"]

        headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
            'Cache-Control': 'no-cache',
            'Connection': 'keep-alive',
            'Pragma': 'no-cache',
            'Referer': 'https://www.wurzelimperium.de/',
            'Sec-Fetch-Dest': 'document',
            'Sec-Fetch-Mode': 'navigate',
            'Sec-Fetch-Site': 'same-site',
            'Upgrade-Insecure-Requests': '1',
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.88 Safari/537.36',
            'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="100", "Google Chrome";v="100"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Linux"',
        }

        response = requests.get(url + "&embed=&ref=&retid=", headers=headers, allow_redirects=False)

        self.PHPSESSID = response.headers["Set-Cookie"].split("PHPSESSID=")[1].split(";")[0]
        self.WUNR = response.headers["Set-Cookie"].split("wunr=")[1]

        # load the garden one time to get some more information
        self.load_garden()




    '''
    Get the garden page to receive information about the player (money, inventory, ...)
    '''
    def load_garden(self):
        cookies = {
            'PHPSESSID': self.PHPSESSID,
            'wunr': self.WUNR,
        }

        headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
            'Cache-Control': 'no-cache',
            'Connection': 'keep-alive',
            'Pragma': 'no-cache',
            'Referer': 'https://www.wurzelimperium.de/',
            'Sec-Fetch-Dest': 'document',
            'Sec-Fetch-Mode': 'navigate',
            'Sec-Fetch-Site': 'same-site',
            'Upgrade-Insecure-Requests': '1',
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.88 Safari/537.36',
            'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="100", "Google Chrome";v="100"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Linux"',
        }

        params = {
            'page': 'garden',
        }

        response = requests.get('https://s' + self.server + '.wurzelimperium.de/main.php', params=params, cookies=cookies, headers=headers)

        self.money = float(str(response.content).split("span id=\"bar\">")[1].split(" wT")[0].replace(",", "."))
        self.points = int(str(response.content).split("span id=\"pkt\">")[1].split("<")[0].replace(".", ""))
        self.inventory = json.loads(str(response.content).split("regal.init(")[1].split(")")[0])["products"]
        self.garden = json.loads(str(response.content).split("\"garden\":")[1].split(",\"water\"")[0])
        self.products = json.loads(str(response.content).split("data_products = ")[1].split(";var")[0])
        self.AJAXTOKEN = str(response.content).split("setToken(\"")[1].split("\")")[0]





    '''
    Harvest all plants that can be harvested
    '''
    def harvest_all(self):
        cookies = {
            'PHPSESSID': self.PHPSESSID,
            'wunr': self.WUNR,
        }

        headers = {
            'Accept': 'text/javascript, text/html, application/xml, text/xml, */*',
            'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
            'Cache-Control': 'no-cache',
            'Connection': 'keep-alive',
            'Pragma': 'no-cache',
            'Referer': 'https://s' + self.server + '.wurzelimperium.de/main.php?page=garden',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-origin',
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.88 Safari/537.36',
            'X-Prototype-Version': '1.7',
            'X-Requested-With': 'XMLHttpRequest',
            'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="100", "Google Chrome";v="100"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Linux"',
        }

        params = {
            'do': 'gardenHarvestAll',
            'token': self.AJAXTOKEN,
        }

        response = requests.get('https://s' + self.server + '.wurzelimperium.de/ajax/ajax.php', params=params, cookies=cookies, headers=headers)

        # refresh data
        self.load_garden()





    '''
    Get a list of the empty fields
    '''
    def get_empty_fields(self):
        empty_fields = []

        for field in self.garden:
            if self.garden[field][0]==0:
                empty_fields.append(field)

        return empty_fields





    '''
    Get the id for the plant name
    '''
    def get_plant_id(self, name):
        for id in self.products:
            if self.products[id]["name"] == name:
                return id





    '''
    Perform the planting by sending the request
    '''
    def plant_fields(self, wanted_plants={}):
        if wanted_plants=={}:
            return -1

        cookies = {
            'PHPSESSID': self.PHPSESSID,
            'wunr': self.WUNR,
        }

        headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:99.0) Gecko/20100101 Firefox/99.0',
            'Accept': 'text/javascript, text/html, application/xml, text/xml, */*',
            'Accept-Language': 'en-US,en;q=0.5',
            'X-Requested-With': 'XMLHttpRequest',
            'X-Prototype-Version': '1.7',
            'DNT': '1',
            'Connection': 'keep-alive',
            'Referer': 'https://s' + self.server + '.wurzelimperium.de/main.php?page=garden',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-origin',
        }

        attach_str = ""
        # iterate over the wanted plantations
        for field in wanted_plants:
            attach_str += "pflanze[]=" + str(wanted_plants[field]) + "&feld[]=" + str(field) + "&felder[]=" + str(field) + "&"

        response = requests.get('https://s' + self.server + '.wurzelimperium.de/save/pflanz.php?' + attach_str + 'cid=' + self.AJAXTOKEN + '&garden=1', cookies=cookies, headers=headers)





    '''
    Water all wanted fields
    '''
    def water_fields(self, wanted_fields=[]):
        if wanted_fields==[]:
            return -1

        cookies = {
            'PHPSESSID': self.PHPSESSID,
            'wunr': self.WUNR,
        }

        headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:99.0) Gecko/20100101 Firefox/99.0',
            'Accept': 'text/javascript, text/html, application/xml, text/xml, */*',
            'Accept-Language': 'en-US,en;q=0.5',
            'X-Requested-With': 'XMLHttpRequest',
            'X-Prototype-Version': '1.7',
            'DNT': '1',
            'Connection': 'keep-alive',
            'Referer': 'https://s' + self.server + '.wurzelimperium.de/main.php?page=garden',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-origin',
        }

        attach_str = ""
        # iterate over the wanted plantations
        for field in wanted_fields:
            attach_str += "feld[]=" + str(field) + "&felder[]=" + str(field) + "&"

        response = requests.get('https://s' + self.server + '.wurzelimperium.de/save/wasser.php?' + attach_str + 'cid=a7213bacafbe5da59352e2d58476bcb3&garden=1', cookies=cookies, headers=headers)





    '''
    Plant as many fields as possible with the specified wanted_plants (and its available seeds)
    '''
    def plant(self, wanted_plants):
        empty_fields = self.get_empty_fields()

        # plant schedule (search for the wanted_plants how many of the empty_fields can be filled and check how many of each are being planted)
        no_empty_fields = len(empty_fields)
        plant_list = []
        for plant in wanted_plants:
            # get available seeds
            if self.get_plant_id(plant) in self.inventory:
                seeds = self.inventory[self.get_plant_id(plant)]
            else:
                continue
            if seeds >= no_empty_fields: #
                plant_list.extend([self.get_plant_id(plant) for i in range(no_empty_fields)])
                break
            else:
                plant_list.extend([self.get_plant_id(plant) for i in range(seeds)])
                no_empty_fields -= seeds

        # iterate over part that fits in packages of 6
        for i in range(int(len(plant_list)/6)):
            do_plants = {}
            for j in range(6):
                do_plants[empty_fields[i*6+j]] = plant_list[i*6+j]
            self.plant_fields(do_plants)

        # remainder that doesnt fit in a package of 6 entries
        rem = len(plant_list)%6
        do_plants = {}
        # take care of the remainder
        for i in range(1,rem+1):
            do_plants[empty_fields[-i]] = plant_list[-i]
        self.plant_fields(do_plants)

        # water all fields
        for i in range(34):
            self.water_fields([i*6 + j for j in range(1,7)])
