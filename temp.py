import utils.wurzel_session as ws
import time


wh_sess = ws.wurzel_session("", "", "2")

count = 1
while True:
    print(f"Planting round: {count}                                     ", end="\r")
    wh_sess.login()

    wh_sess.harvest_all()
    time.sleep(1)
    wh_sess.plant(["Karotte"])

    time.sleep(600)
    count += 1
